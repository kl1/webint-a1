To run:
1. install rustup: https://rustup.rs
2. install Rust Beta: ```rustup install beta```
3. compile and run (from the git root): ```cargo +beta run```