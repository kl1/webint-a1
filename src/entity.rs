#[derive(Serialize, Debug)]
pub struct User {
    pub name: String,
    pub id: u32,
    pub ratings: Vec<Rating>,
}

impl User {
    /// Returns the rating pairs that are common for this user and the other user.
    pub fn common_ratings<'a>(&'a self, other: &'a User) -> Vec<(&Rating, &Rating)> {
        self.ratings
            .iter()
            .filter_map(|r| {
                let same = other.ratings.iter().find(|r2| r.movie == r2.movie)?;
                Some((r, same))
            }).collect()
    }
}

#[derive(Serialize, Debug)]
pub struct Rating {
    pub user_id: u32,
    pub movie: String,
    pub score: f64,
}
