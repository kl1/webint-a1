use crate::entity::*;
use std::error::Error;

fn parse_csv<T, F>(path: &str, constructor: F) -> Result<Vec<T>, Box<Error>>
where
    F: Fn(csv::StringRecord) -> Option<T>,
{
    let mut reader = csv::ReaderBuilder::new().delimiter(b';').from_path(path)?;

    let mut result = vec![];
    for record in reader.records() {
        let record = record?;
        let parsed = constructor(record).ok_or("csv parse error")?;
        result.push(parsed);
    }

    Ok(result)
}

pub fn read_users() -> Result<Vec<User>, Box<Error>> {
    let mut users = parse_csv("res/users.csv", |record| {
        let name = record.get(0)?.to_string();
        let id = record.get(1)?.parse().ok()?;

        Some(User {
            name,
            id,
            ratings: vec![],
        })
    })?;

    add_ratings(&mut users)?;

    Ok(users)
}

fn add_ratings(users: &mut [User]) -> Result<(), Box<Error>> {
    for rating in read_ratings()?.into_iter() {
        if let Some(user) = users.iter_mut().find(|u| u.id == rating.user_id) {
            user.ratings.push(rating);
        }
    }

    Ok(())
}

fn read_ratings() -> Result<Vec<Rating>, Box<Error>> {
    parse_csv("res/ratings.csv", |record| {
        Some(Rating {
            user_id: record.get(0)?.parse().ok()?,
            movie: record.get(1)?.to_string(),
            score: record.get(2)?.parse().ok()?,
        })
    })
}
