use crate::entity::*;
use std::cmp::Ordering;
use std::collections::HashMap;

pub struct Stats<'a> {
    pub users: &'a Vec<User>,
}

// Is used when calculating the weighted movie scores. The similarity is the similarity to the
// user that we are finding the recommendations for.
#[derive(Debug)]
struct MovieScore<'a> {
    user: &'a User,
    sim: f64,
    ratings: Vec<&'a Rating>,
}

// Is used when totaling up the scores into a total sim value and a total weighted score value.
#[derive(Debug)]
struct Total {
    sim: f64,
    score: f64,
}

impl<'a> Stats<'a> {
    pub fn euclidean_users(&self, user: &User) -> Vec<(&User, f64)> {
        self.distances(user, calc_euclidean)
    }

    pub fn pearson_users(&self, user: &User) -> Vec<(&User, f64)> {
        self.distances(user, calc_pearson)
    }

    pub fn euclidean_movies(&self, user: &User) -> Vec<(&str, f64)> {
        self.weighted_scores(user, calc_euclidean)
    }

    pub fn pearson_movies(&self, user: &User) -> Vec<(&str, f64)> {
        self.weighted_scores(user, calc_pearson)
    }

    fn distances<T>(&self, user: &User, sim_fn: T) -> Vec<(&User, f64)>
    where
        T: Fn(&User, &User) -> f64,
    {
        let mut distances: Vec<_> = self
            .users
            .iter()
            .filter(|u| u.id != user.id)
            .map(|u| (u, sim_fn(user, u)))
            .collect();

        sort_scores(&mut distances);

        distances
    }

    fn weighted_scores<T>(&self, user: &User, sim_fn: T) -> Vec<(&str, f64)>
    where
        T: Fn(&User, &User) -> f64,
    {
        let scores = self.movie_scores(user, sim_fn);

        let mut weighted: Vec<(&str, f64)> = weigh_scores(scores).into_iter().collect();

        sort_scores(&mut weighted);

        weighted
    }

    // For a given user return a vec that contains the MovieScore entry for every other user.
    fn movie_scores<T>(&self, user: &User, sim_fn: T) -> Vec<MovieScore>
    where
        T: Fn(&User, &User) -> f64,
    {
        self.users
            .iter()
            .filter(|u| u.id != user.id)
            .filter_map(|u| {
                let unseen: Vec<&Rating> = u
                    .ratings
                    .iter()
                    .filter(|r| !user.ratings.iter().any(|r2| r2.movie == r.movie))
                    .collect();

                if unseen.is_empty() {
                    None
                } else {
                    Some(MovieScore {
                        user: u,
                        sim: sim_fn(user, u),
                        ratings: unseen,
                    })
                }
            }).collect()
    }
}

fn sort_scores<T>(scores: &mut [(T, f64)]) {
    scores.sort_unstable_by(|a, b| a.1.partial_cmp(&b.1).unwrap_or(Ordering::Equal).reverse());
}

fn weigh_scores(scores: Vec<MovieScore>) -> HashMap<&str, f64> {
    scores
        .iter()
        .fold(HashMap::<&str, Total>::new(), |mut acc, score| {
            for &rating in score.ratings.iter() {
                let weighed_score = rating.score * score.sim;
                acc.entry(&rating.movie)
                    .and_modify(|e| {
                        e.sim += score.sim;
                        e.score += weighed_score;
                    }).or_insert(Total {
                        sim: score.sim,
                        score: weighed_score,
                    });
            }
            acc
        }).into_iter()
        .map(|kv| (kv.0, kv.1.score / kv.1.sim))
        .collect()
}

// Gets the common ratings for two users or returns 0.0 if they have no ratings in common.
macro_rules! common_or_return_zero {
    ($a:ident, $b:ident) => {{
        let common = $a.common_ratings($b);
        if common.is_empty() {
            return 0.0;
        }
        common
    }};
}

fn calc_euclidean(a: &User, b: &User) -> f64 {
    let common = common_or_return_zero!(a, b);

    let sim = common.iter().fold(0.0, |mut acc, r| {
        acc += (r.0.score - r.1.score).powf(2.0);
        acc
    });

    1.0 / (1.0 + sim)
}

fn calc_pearson(a: &User, b: &User) -> f64 {
    let common = common_or_return_zero!(a, b);

    let (scores_a, scores_b) = common.iter().fold((vec![], vec![]), |mut acc, r| {
        acc.0.push(r.0.score);
        acc.1.push(r.1.score);
        acc
    });

    covariance(&scores_a, &scores_b) / (stddev(&scores_a) * stddev(&scores_b))
}

// stat helpers

fn mean(numbers: &[f64]) -> f64 {
    numbers.iter().sum::<f64>() / numbers.len() as f64
}

// Calculates the covariance of two slices of numbers. If the length of the slices are not equal
// the numbers that are present in the longer but not in the shorter slice are ignored.
fn covariance(samples_a: &[f64], samples_b: &[f64]) -> f64 {
    let length = std::cmp::min(samples_a.len(), samples_b.len());
    if length == 0 {
        return 0.0;
    };

    let mean_a = mean(&samples_a);
    let mean_b = mean(&samples_b);

    samples_a
        .iter()
        .zip(samples_b.iter())
        .map(|s| (s.0 - mean_a) * (s.1 - mean_b))
        .sum::<f64>()
        / length as f64
}

fn stddev(samples: &[f64]) -> f64 {
    let length = samples.len();
    if length == 0 {
        return 0.0;
    }

    let mean_s = mean(&samples);

    let squared_average =
        samples.iter().map(|s| (s - mean_s).powf(2.0)).sum::<f64>() / length as f64;

    squared_average.sqrt()
}

/*
fn calc_pearson2(a: &User, b: &User) -> f64 {
    let mut sum1 = 0.0;
    let mut sum2 = 0.0;
    let mut sum1sq = 0.0;
    let mut sum2sq = 0.0;
    let mut psum = 0.0;
    let mut n = 0;

    for rat_a in &a.ratings {
        for rat_b in &b.ratings {
            if rat_a.movie == rat_b.movie {
                sum1 += rat_a.score;
                sum2 += rat_b.score;
                sum1sq += rat_a.score.powf(2.0);
                sum2sq += rat_b.score.powf(2.0);
                psum += rat_a.score * rat_b.score;
                n += 1;
            }
        }
    }

    if n == 0 {
        return 0.0;
    }

    let num = psum - (sum1 * sum2 / n as f64);
    let den = ((sum1sq - sum1.powf(2.0) / n as f64) * (sum2sq - sum2.powf(2.0) / n as f64)).sqrt();
    num / den
}
*/
