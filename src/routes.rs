use crate::entity::User;
use crate::parser;
use crate::stats::Stats;
use gotham::helpers::http::response::{create_empty_response, create_response};
use gotham::state::{FromState, State};
use std::collections::HashMap;
use tera::{Context, Tera};

use hyper::{Body, Response, StatusCode};

lazy_static! {
    static ref TERA: Tera = { compile_templates!("templates/**/*") };
}

lazy_static! {
    static ref USERS: Vec<User> = { parser::read_users().unwrap() };
}

lazy_static! {
    static ref USER_MAP: HashMap<u32, &'static User> =
        { USERS.iter().map(|u| (u.id, u)).collect() };
}

lazy_static! {
    static ref STATS: Stats<'static> = { Stats { users: &*USERS } };
}

// /

pub fn root(state: State) -> (State, Response<Body>) {
    let mut context = Context::new();
    context.insert("users", &*USERS);
    let html = TERA.render("main.html", &context).unwrap();

    let res = create_html_response(&state, html);
    (state, res)
}

// /recommend

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct RecommendExtractor {
    pub user_id: u32,
    pub algo: String,
}

pub fn recommend(mut state: State) -> (State, Response<Body>) {
    let RecommendExtractor { user_id, algo } = RecommendExtractor::take_from(&mut state);

    if let Some(user) = USER_MAP.get(&user_id) {
        let (rec_users, rec_movies) = get_recommendations(&user, &algo);

        let mut context = Context::new();
        context.insert("algo", &algo);
        context.insert("user", &user);
        context.insert("rec_users", &rec_users);
        context.insert("rec_movies", &rec_movies);
        context.insert("rec_movies_len", &rec_movies.len());

        let res = create_html_response(&state, TERA.render("recommend.html", &context).unwrap());

        (state, res)
    } else {
        let res = create_empty_response(&state, StatusCode::NOT_FOUND);
        (state, res)
    }
}

fn get_recommendations(
    user: &User,
    algo: &str,
) -> (Vec<(&'static User, f64)>, Vec<(&'static str, f64)>) {
    let (rec_users, rec_movies) = if algo == "euclidean" {
        (STATS.euclidean_users(&user), STATS.euclidean_movies(&user))
    } else {
        (STATS.pearson_users(&user), STATS.pearson_movies(&user))
    };

    // only use the first 3
    (
        rec_users.into_iter().take(3).collect(),
        rec_movies.into_iter().take(3).collect(),
    )
}

// Helpers

fn create_html_response<B>(state: &State, body: B) -> Response<Body>
where
    B: Into<Body>,
{
    create_response(state, StatusCode::OK, mime::TEXT_HTML, body)
}
